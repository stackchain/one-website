const gulp = require('gulp-help')(require('gulp'));
const postcss = require('gulp-postcss');

gulp.task('postcss', 'build postcss files', function() {
  const plugins = [
    require('postcss-import')(),
    require('autoprefixer')(),
    require('postcss-calc')(),
    require('postcss-color-function')(),
    require('postcss-custom-properties')({
      preserve: false
    }),
    require('postcss-css-variables')({
      preserve: false
    }),
    require('postcss-discard-comments')(),
    require('postcss-custom-media')({
      preserve: false
    }),
    require('cssnano')({zindex: false}),
  ];
  const replace = require('gulp-replace');
  const concat = require('gulp-concat');
  const options = {};
  return gulp.src(['src/css/**/*.css', '!src/css/**/_*.css'])
      .pipe(postcss(plugins, options))
      .pipe(replace('!important', ''))
      .pipe(concat('css.html.mustache'))
      .pipe(gulp.dest('build/pre-render/templates/partials'))
});