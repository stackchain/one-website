var fs = require("fs");
var path = require("path");

const getNextFiles = function (srcPath, debug) {
  const folder = fs.readdirSync(srcPath);
  const files = folder.filter((f, i) => {
    return fs.statSync(path.join(srcPath, f)).isFile();
  });
  if (debug) console.log("[DEBUG] srcPath loaded --->", files.length);
  return files;
}

const jsoner = function (file, debug) {
  if (debug) console.log("[DEBUG] baseJSON loaded -->", file);
  return JSON.parse(fs.readFileSync(file)) 
};

const buildFiles = function (baseJSONFile, srcPath, files, arrayTag, destPath, preserve, debug) {
  let baseJSON = jsoner(baseJSONFile, debug);
  files.forEach(function (file) {
    let base = (preserve ? baseJSON : JSON.parse(JSON.stringify(baseJSON)));

    let point = arrayTag.split(".").reduce(function(acc, value) {
      if (!value) return base[acc];
      return (typeof acc === "string" ? base[acc][value] : acc[value]);
    });

    if(!Array.isArray(point)) {
      console.log("tagPath must point to an array!");
      process.exit(1);
    }

    if (debug) console.log("[DEBUG] data loaded -->", path.join(srcPath, file));
    point.push(jsoner(path.join(srcPath, file)));

    let dstFile = (preserve ? baseJSONFile : path.join(destPath, file));

    if(debug) console.log("[DEBUG] data pushed -->", point.length);

    fs.writeFileSync(dstFile, JSON.stringify(base));
    if(debug) console.log("[DEBUG] file saved --->", dstFile);
  });
}

require("yargs")
  .demand(1)
  .command(
    "build <baseJSON> <srcPath> <destPath> <tagPath> [preserve] [debug]",
    "Generate X JSONs from srcPath with baseJSON adding in baseJSON on tagPath the JSONs files within srcPath",
    {},
    function(options) {
      if(options.debug) console.log("[DEBUG] options loaded -->", options);
      buildFiles(options.baseJSON, options.srcPath, getNextFiles(options.srcPath, options.debug), options.tagPath, options.destPath, options.preserve, options.debug);
    })
  .example(
    "node $0 build ../template.json ../path/data ../build/render blog-api.posts.articles",
    "Build files to destPath iterating in path/data using template as base")
  .wrap(120)
  .recommendCommands()
  .epilogue("For more info - read the code :)")
  .help()
  .strict()
  .argv;
