module.exports = {
  "globDirectory": "build/public",
  "globPatterns": [
    "**/*.{js,css,png,jpg,svg,ico,html}"
  ],
  "swSrc": "sw.js",
  "swDest": "build/public/sw.js"
};
