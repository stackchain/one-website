module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['build/', 'public/', 'dist/'],
    copy: {
      before: {
        files: [
          {expand: true, cwd: 'src/media', src: ['**'], dest: 'build/site/media'},
          {expand: true, cwd: 'src', src: ['*'], dest: 'build/site', filter: 'isFile'},
          {expand: true, src: ['src/js/*.js'], dest: 'build/site/js', filter: 'isFile', flatten: true},
          {expand: true, cwd: 'src/templates', src: ['**'], dest: 'build/pre-render/templates'},
          {expand: true, cwd: 'src/fixtures', src: ['**'], dest: 'build/pre-render/fixtures'},
        ],
      },
      preview: {
        files: [
          {expand: true, cwd: 'src/fixtures/i18n/articles/en-US',
          src: [
            'how-crypto-wallets-work-and-their-different-security-levels.json',
            'what-is-blockchain-and-how-blockchain-works.json'
          ], dest: 'build/pre-render/preview/en-US', filter: 'isFile', flatten: true},
          {expand: true, cwd: 'src/fixtures/i18n/articles/pt-BR',
          src: [
            'como-funcionam-as-carteiras-cripto-e-o-seus-diferentes-niveis-de-seguranca.json',
            'o-que-e-blockchain-e-como-funciona-uma-blockchain.json'
          ], dest: 'build/pre-render/preview/pt-BR', filter: 'isFile', flatten: true}
        ]
      },
      index: {
        files: [
          {expand: true, src: ['build/render/en-US/index.html'], rename: function() { return 'build/public/index.en_US.amp.html'; }},
          {expand: true, src: ['build/render/pt-BR/index.html'], rename: function() { return 'build/public/index.pt_BR.amp.html'; }},
        ]
      },
      blog_listing: {
        files: [
          {expand: true, src: ['build/render/en-US/index.html'], rename: function() { return 'build/public/blog.en_US.amp.html'; }},
          {expand: true, src: ['build/render/pt-BR/index.html'], rename: function() { return 'build/public/blog.pt_BR.amp.html'; }},
        ]
      },
      what_we_do: {
        files: [
          {expand: true, src: ['build/render/en-US/index.html'], rename: function() { return 'build/public/what-we-do.en_US.amp.html'; }},
          {expand: true, src: ['build/render/pt-BR/index.html'], rename: function() { return 'build/public/o-que-nos-fazemos.pt_BR.amp.html'; }},
        ]
      },
      who_we_are: {
        files: [
          {expand: true, src: ['build/render/en-US/index.html'], rename: function() { return 'build/public/who-we-are.en_US.amp.html'; }},
          {expand: true, src: ['build/render/pt-BR/index.html'], rename: function() { return 'build/public/quem-somos.pt_BR.amp.html'; }},
        ]
      },
      articles: {
        files: [
          {expand: true, cwd: 'build/render/articles', src: ['**'], dest: 'build/public', filter: 'isFile', rename: function(dst, src) {
            let locale = src.substr(0, src.indexOf("/"));
            let file = dst + src.substr(src.indexOf("/"));
            let parts = file.split(".");
            let name = [parts[0], locale.replace("-", "_"), "amp", parts[1]].join(".");
            return name;
          }}
        ]
      },
      after: {
        files: [
          {expand: true, cwd: 'build/site/media', src: ['**'], dest: 'build/public/media'}, // clean images compress etc hash
          {expand: true, cwd: 'build/site', src: ['*'], dest: 'build/public', filter: 'isFile'}, // compress uglify etc
          {expand: true, src: ['build/site/media/icons/onepercent-favicon.ico'], rename: function() { return 'build/public/favicon.ico'; }}
        ]
      }
    },
    mustache_render: {
      index: {
        options: {
          directory: "build/pre-render/templates/partials/",
          tags: ['<%', '%>']
        },
        files: [{
          expand: true,
          flatten: false,
          cwd: 'build/pre-render/fixtures/i18n/main',
          src: '**/*.json',
          template: 'build/pre-render/templates/index.html.mustache',
          dest: 'build/render',
          ext: '.html',
          extDot: 'last'
        }]
      },
      what_we_do: {
        options: {
          directory: "build/pre-render/templates/partials/",
          tags: ['<%', '%>']
        },
        files: [{
          expand: true,
          flatten: false,
          cwd: 'build/pre-render/fixtures/i18n/main',
          src: '**/*.json',
          template: 'build/pre-render/templates/what-we-do.html.mustache',
          dest: 'build/render',
          ext: '.html',
          extDot: 'last'
        }]
      },
      who_we_are: {
        options: {
          directory: "build/pre-render/templates/partials/",
          tags: ['<%', '%>']
        },
        files: [{
          expand: true,
          flatten: false,
          cwd: 'build/pre-render/fixtures/i18n/main',
          src: '**/*.json',
          template: 'build/pre-render/templates/who-we-are.html.mustache',
          dest: 'build/render',
          ext: '.html',
          extDot: 'last'
        }]
      },
      blog_listing: {
        options: {
          directory: "build/pre-render/templates/partials/",
          tags: ['<%', '%>']
        },
        files: [{
          expand: true,
          flatten: false,
          cwd: 'build/pre-render/fixtures/i18n/main',
          src: '**/*.json',
          template: 'build/pre-render/templates/blog.html.mustache',
          dest: 'build/render',
          ext: '.html',
          extDot: 'last'
        }]
      },
      blog_articles: {
        options: {
          directory: "build/pre-render/templates/partials/",
          tags: ['<%', '%>']
        },
        files: [{
          expand: true,
          flatten: false,
          cwd: 'build/pre-render/fixtures/i18n/articles',
          src: '**/*.json',
          template: 'build/pre-render/templates/blog-article.html.mustache',
          dest: 'build/render/articles',
          ext: '.html',
          extDot: 'last'
        }]
      }
    },
    postcss: {
      build: {
        src: ['src/css/ampstart-base/page.css', '!src/css/**/_*.css'],
        dest: 'build/css/style._css',
        options: {
          map: false,
          processors: [
            require('postcss-import')(),
            require('postcss-cssnext')(),
            require('postcss-discard-comments')(),
            require('postcss-reporter')()
          ],
        }
      },
      prod: {
        src: ['build/css/*.css'],
        dest: 'build/pre-render/templates/partials/css.html.mustache',
        options: {
          map: false,
          use: [
            
          ],
          processors: [
            require('cssnano')({
              zindex: false
            }),
          ]
        }
      }
    },
    watch: {
      html: {
        files: 'src/**',
        tasks: ['build'],
      },
      options: {
        livereload: true,
      },
    },
    exec: {
      remove_important_css: "cat build/css/style._css | sed s/\!important//g > build/css/style.css",
      build_css: "gulp postcss",
      build_jsons: "npm run preview:en_US && npm run preview:pt_BR && npm run articles:en_US && npm run articles:pt_BR",
      workbox: "workbox injectManifest workbox-config.js",
      validate_amp: "amphtml-validator build/public/*.amp.html"
    },
    connect: {
      server: {
        options: {
          base: 'build/public',
          livereload: true,
          port: 3000,
          debug: true,
          protocol: "https"
        },
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-mustache-render');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-exec');

  // Default task(s).
  grunt.registerTask('default', ['connect', 'watch']);
  grunt.registerTask('build', [
    'clean', 
    'copy:before', 
    'copy:preview',
    'postcss:build',
    'exec:remove_important_css',
    'postcss:prod',
    'exec:build_jsons',
    'mustache_render:index', 
    'copy:index', 
    'mustache_render:blog_listing',
    'copy:blog_listing',
    'mustache_render:what_we_do',
    'copy:what_we_do',
    'mustache_render:who_we_are',
    'copy:who_we_are',
    'mustache_render:blog_articles',
    'copy:articles',
    'copy:after',
    'exec:workbox',
    'exec:validate_amp'
  ]);
  grunt.registerTask('prod', ['postcss:prod']);
};
