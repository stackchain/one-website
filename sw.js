importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);

  workbox.core.setCacheNameDetails({
    prefix: 'oneweb',
    suffix: 'v1'
  });

  workbox.googleAnalytics.initialize();

  workbox.precaching.precacheAndRoute([]);

  self.addEventListener('install', event => {
    const urls = [
      'https://cdn.ampproject.org/v0.js',
      'https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js',
      'https://cdn.ampproject.org/shadow-v0.js',
      'https://cdn.ampproject.org/v0/amp-sidebar-0.1.js',
      'https://cdn.ampproject.org/v0/amp-analytics-0.1.js',
      'index.en_US.amp.html',
      'index.pt_BR.amp.html',
      'manifest.json',
      'offline.html',
      'install-service-worker.html',
      'browserconfig.xml'
    ];
    const cacheName = workbox.core.cacheNames.runtime;
    event.waitUntil(caches.open(cacheName).then(cache => cache.addAll(urls)));
  });

  workbox.routing.registerRoute(
    new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'webfonts',
      plugins: [
        new workbox.expiration.Plugin({
          maxAgeSeconds: 14 * 24 * 60 * 60 // two weeks
        })
      ]
    })
  );

  workbox.routing.registerRoute(/(.*)html|(.*)\/$/, args => {
    return workbox.strategies.networkFirst().handle(args).then(response => {
      if (!response) {
        return caches.match('offline.html');
      }
      return response;
    });
  });

  workbox.routing.registerRoute(/\.(?:js|css|png|gif|jpg|svg)$/,
    workbox.strategies.cacheFirst({
      cacheName: 'static',
      plugins: [
        new workbox.expiration.Plugin({
          maxAgeSeconds: 7 * 24 * 60 * 60 // one week
        })
      ]
    })
  );

  workbox.routing.registerRoute(/(.*)cdn\.ampproject\.org(.*)/,
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'amp',
      plugins: [
        new workbox.expiration.Plugin({
          maxAgeSeconds: 14 * 24 * 60 * 60 // two weeks
        })
      ]
    })
  );

} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
